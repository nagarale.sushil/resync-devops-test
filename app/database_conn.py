import sqlite3




class database_conn:

    def __init__(self):
        print("initialized the database")


    def executesql(self, query):
        try:
            conn = sqlite3.connect('main.db')

            print(f"Running query => {query}")
            print("Successfully connecte to database")
            cursor  = conn.execute(query)
            columns = cursor.description
            result = []
            for row in cursor:
                tmp={}
                for (index,column) in enumerate(row):
                    tmp[columns[index][0]] = column
                result.append(tmp)
            conn.commit()
            conn.close()
            return result

        except Exception as error:
            print("Error while connecting to SQLITE", error)
            return error
        finally:
            if (conn):
                conn.close()
                print("Sqlite connection is closed") 


    def executesql_noresponse(self, query):
        try:
            success = self.executesql(query)
            query = "commit"
            success = self.executesql(query)
            response = {"result":"success"} 
            return response
        except Exception as error:
            print("Error while connecting to SQLITE", error)
            return error




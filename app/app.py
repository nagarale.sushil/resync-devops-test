from flask import Flask
from flask import json
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from database_conn import database_conn 
import json

application = Flask(__name__)
# application.config['BUNDLE_ERRORS'] = True
api = Api(application)
CORS(application, send_wildcard=True)

with open("./users.json", "r") as f:
    users = json.load(f)


class arithmetic(Resource):
    def post(self):
        try:
            print(reqparse.RequestParser())
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('x', type=int, required=True, location='args', help='X Value')
            self.parser.add_argument('y', type=int, required=True, location='args', help='Y Value')
            self.parser.add_argument('operation', type=str, location='args', help='Mathematical Operators')
            self.args = self.parser.parse_args()

            x = (self.args['x'])
            y = (self.args['y'])
            ops = (self.args['operation'])

            if ops == '-':
                return {'results': x - y}
            elif ops == '*':
                return {'results': x * y}
            elif ops == '/':
                return {'results': x / y}
            else:
                return {'results': x + y}
            
        except Exception as e:
            return {'status': str(e)}

class Users(Resource):
    def get(self):
        ## TODO: Get Users either by id or get all Users
        try:
            dc = database_conn()
            query = "SELECT * FROM users "

            self.parser = reqparse.RequestParser()
            self.parser.add_argument('userid', type=str, required=False, location='args')
            self.args = self.parser.parse_args()

            userid  = (self.args['userid'])
            if(userid):
                query = f"SELECT * FROM users where id = '{userid}'"

            response = dc.executesql(query)
            response = application.response_class(
            response=json.dumps(response),
            mimetype='application/json'
            )
            return response 
        except Exception as e:
            return {'status': str(e)}
        
    
    def post(self):
        ## TODO: Create User
        try:
            dc = database_conn()
            

            self.parser = reqparse.RequestParser()
            self.parser.add_argument('userid', type=str, required=True, location='args', help='Enter Userid')
            self.parser.add_argument('username', type=str, required=True, location='args', help='Enter User Name')
            self.parser.add_argument('userdesc', type=str, required=True, location='args', help='Enter User Description')
            self.args = self.parser.parse_args()

            userid  = (self.args['userid'])
            username = (self.args['username'])
            userdesc = (self.args['userdesc'])
            
            query = f"INSERT INTO users values ('{userid}','{username}','{userdesc}')"

            response = dc.executesql_noresponse(query)

            response = application.response_class(
            response=json.dumps(response),
            mimetype='application/json'
            )
            return response 
        except Exception as e:
            return {'status': str(e)}
        
        print("post request")
    
    def patch(self):
        ## TODO: Update User
        try:
            dc = database_conn()
            

            self.parser = reqparse.RequestParser()
            self.parser.add_argument('userid', type=str, required=True, location='args', help='Enter Userid')            
            self.parser.add_argument('updatefield', type=str, required=True, location='args', help='Enter id / name / description')
            self.parser.add_argument('updateval', type=str, required=True, location='args', help='Enter Update field value')
            self.args = self.parser.parse_args()

            userid  = (self.args['userid'])
            updatefield = (self.args['updatefield'])
            updateval = (self.args['updateval'])
            fields = ["id","name","description"]
            if updatefield in fields:
                query = f"UPDATE users SET {updatefield} = '{updateval}' WHERE id = '{userid}'"
            else:
                raise Exception("Invalid update field")

            response = dc.executesql_noresponse(query)

            response = application.response_class(
            response=json.dumps(response),
            mimetype='application/json'
            )
            return response 

        except Exception as e:
            return {'status': str(e)}        
        print("update request")
    
    def delete(self):
        ## TODO: Delete User
        try:
            dc = database_conn()
            

            self.parser = reqparse.RequestParser()
            self.parser.add_argument('userid', type=str, required=True, location='args', help='Enter Userid')
            self.args = self.parser.parse_args()

            userid  = (self.args['userid'])
            query = f"DELETE FROM users WHERE id  = '{userid}'"

            response = dc.executesql_noresponse(query)

            response = application.response_class(
            response=json.dumps(response),
            mimetype='application/json'
            )
            return response 

        except Exception as e:
            return {'status': str(e)}        
        print("delete request)")

        
        
api.add_resource(arithmetic, '/arithmetic')
api.add_resource(Users, '/users')

if __name__ == '__main__':
    application.run(host='0.0.0.0', threaded=True)
# syntax=docker/dockerfile:1
FROM python:3.9.4
WORKDIR /Users/sushilnagarale/Resync Test/resync-devops-test
ENV FLASK_APP="/Users/sushilnagarale/Resync Test/resync-devops-test/app/app.py"
ENV FLASK_RUN_HOST=0.0.0.0
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]